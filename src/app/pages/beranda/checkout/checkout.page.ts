import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Platform } from '@ionic/angular';

declare var google;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: [ './checkout.page.scss' ],
})
export class CheckoutPage implements OnInit {

  public map;
  @ViewChild('mapElement') mapElement;

  pesanan: any = [];
  total_bayar: number = 0;

  constructor(
    public platform: Platform,
    public route: ActivatedRoute,
    public router: Router,
    private zone: NgZone,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.pesanan = JSON.parse(params.pesanan);

      for (let i = 0; i < this.pesanan.length; i++) {
        this.total_bayar += this.pesanan[ i ].harga * this.pesanan[ i ].kuantiti
      }
    });
  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      let map = new google.maps.Map(this.mapElement.nativeElement, {
        center: { lat: -7.534044, lng: 110.818653 },
        disableDefaultUI: true,
        draggable: true,
        zoom: 18
      });

      //-----
      this.zone.run(async () => {
        this.getMapInfo(map.getCenter().lat(), map.getCenter().lng())

        google.maps.event.addListener(map, 'dragend', function () {
          var latLng = new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng());

          var geocoder = new google.maps.Geocoder;
          geocoder.geocode({ 'location': latLng }, function (results, status) {
            if (status !== 'OK') { alert('Geocoder failed due to: ' + status); return; }
            document.getElementById('test').innerHTML = results[ 0 ].formatted_address;
          });
        });
      });
    });
  }

  getMapInfo(val_lat, val_lng) {
    var latLng = new google.maps.LatLng(val_lat, val_lng);

    var geocoder = new google.maps.Geocoder;
    geocoder.geocode({ 'location': latLng }, function (results, status) {
      if (status !== 'OK') { alert('Geocoder failed due to: ' + status); return; }
      document.getElementById('test').innerHTML = results[ 0 ].formatted_address;
    });
  }

}
