import { Component, OnInit } from '@angular/core';
import { AnimationController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-beranda',
  templateUrl: './beranda.page.html',
  styleUrls: [ './beranda.page.scss' ],
})
export class BerandaPage implements OnInit {

  data: any = [];

  total_bayar: number = 0;
  total_kuantiti: number = 0;

  pesanan: any = [];

  constructor(
    public animationCtrl: AnimationController,
    public router: Router
  ) {

  }

  ngOnInit() {
    this.total_bayar = 0;
    this.total_kuantiti = 0;

    for (let i = 1; i <= 10; i++) {
      this.data.push({ id: i, foto: '', nama_produk: 'Nama Produk ' + i, harga: i * 1000, kuantiti: 0 });
    }

    if (this.total_kuantiti == 0) {
      this.animationCtrl.create()
        .addElement(document.querySelector('.ani-popup'))
        .duration(0)
        .fromTo('transform', 'translateY(100px)', 'translateY(100px)').play();
    }
  }

  tambah(val_id, val_harga) {
    if (this.total_kuantiti == 0) {
      this.animationCtrl.create()
        .addElement(document.querySelector('.ani-popup'))
        .duration(230)
        .fromTo('transform', 'translateY(100px)', 'translateY(0px)').play();
    }

    var id = this.data.findIndex(x => x.id == val_id);
    this.data[ id ].kuantiti += 1;
    this.total_kuantiti += 1;
    this.total_bayar += val_harga;
  }

  kurang(val_id, val_harga, val_kuantiti) {
    if (val_kuantiti > 0) {
      var id = this.data.findIndex(x => x.id == val_id);
      this.data[ id ].kuantiti -= 1;
      this.total_kuantiti -= 1;
      this.total_bayar -= val_harga;

      if (this.total_kuantiti == 0) {
        if (this.total_kuantiti == 0) {
          this.animationCtrl.create()
            .addElement(document.querySelector('.ani-popup'))
            .duration(230)
            .fromTo('transform', 'translateY(0px)', 'translateY(100px)').play();
        }
      }
    }
  }

  pesan() {
    this.pesanan=[];
    
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[ i ].kuantiti > 0) {
        this.pesanan.push({
          foto: '',
          nama_produk: this.data[ i ].nama_produk,
          harga: this.data[ i ].harga,
          kuantiti: this.data[ i ].kuantiti
        });
      }
    }

    this.router.navigate([ 'checkout' ],
      {
        queryParams: {
          pesanan: JSON.stringify(this.pesanan)
        }
      }
    );
  }
}