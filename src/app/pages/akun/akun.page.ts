import { ActionSheetController, AlertController, LoadingController, NavController, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Component,  OnInit } from '@angular/core';

import * as firebase from 'firebase';

@Component({
  selector: 'app-akun',
  templateUrl: './akun.page.html',
  styleUrls: [ './akun.page.scss' ],
})
export class AkunPage implements OnInit {

  avatar: string = "assets/images/user.png";
  captureDataUrl: string;

  constructor(
    public actionSheetController: ActionSheetController,
    public alertCtrl: AlertController,
    public camera: Camera,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public toastController: ToastController,
  ) {
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    let storageRef = firebase.storage().ref();
    let imageRef = storageRef.child('seller/avatar.jpg');

    imageRef.getDownloadURL().then(function (url) {
      document.getElementById('avatar').style.backgroundImage = "url('" + url + "')";
    })
  }

  async update_photo() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [ {
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      } ]
    });
    await actionSheet.present();
  }

  openCamera() {
    const cameraOptions: CameraOptions = {
      quality: 90,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetHeight: 200,
      targetWidth: 200,
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {

      this.avatar = "assets/gifs/laoding.gif";
      this.captureDataUrl = 'data:image/jpeg;base64,' + imageData;

      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('seller/avatar.jpg');

      imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL).then((snapshot) => {
        snapshot.ref.getDownloadURL().then(function (downloadURL) {
          document.getElementById('avatar').style.backgroundImage = "url('" + downloadURL + "')";
        });
      });

    }, (err) => {
      console.error('err : ' + err);
      alert('err : ' + JSON.stringify(err))

      this.avatar = "assets/images/user.png";
    });
  }

  openGallery() {
    let options = {
      quality: 90,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetHeight: 200,
      targetWidth: 200,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.avatar = "assets/gifs/laoding.gif";
      this.captureDataUrl = 'data:image/jpeg;base64,' + imagePath;

      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('seller/avatar.jpg');

      imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL).then((snapshot) => {
        snapshot.ref.getDownloadURL().then(function (downloadURL) {
          document.getElementById('avatar').style.backgroundImage = "url('" + downloadURL + "')";
        });
      });
    }, (err) => {
      console.error('err : ' + err);
      alert('err : ' + JSON.stringify(err))

      this.avatar = "assets/images/user.png";
    });
  }

}
