import { Component, OnInit } from '@angular/core';
import { PesananService, Pesanan } from '../../services/pesanan.service';
import { AkunService, Akun } from '../../services/akun.service';
import { Observable } from 'rxjs';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.page.html',
  styleUrls: [ './pesanan.page.scss' ],
})
export class PesananPage implements OnInit {

  data: any = [];

  loading: any;

  pesananStatus: any = {
    id: '',
    status: '',
  };

  sec: number = 0;

  constructor(
    public akunService: AkunService,
    public loadingController: LoadingController,
    public pesananService: PesananService,
  ) { }

  async ngOnInit() {
    this.loading = await this.loadingController.create({})
  }

  ionViewDidEnter() {
    this.load_data();
  }

  load_data() {
    this.data = [];

    let nama: string = '';
    let nomor_hp: string = '';

    this.pesananService.getpesanan().subscribe(snap_pesanan => {
      this.akunService.getAkun().subscribe(snap_akun => {
        if (snap_akun[ 0 ].id == snap_pesanan[ 0 ].id_pelanggan) {
          nama = snap_akun[ 0 ].nama;
          nomor_hp = snap_akun[ 0 ].nomor_hp;
        }

        this.data.push({
          pembeli_nama: nama,
          pembeli_nomor_hp: nomor_hp,
          data: snap_pesanan[ 0 ]
        });

        this.loading.dismiss();

        // alert(JSON.stringify(this.data));
      });
    });
  }

  launch_map() {
    let destination = this.data[ 0 ].lokasi.latitude + ',' + this.data[ 0 ].lokasi.longitude;

    // let label = encodeURI('Lokasi');
    // window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');

    let label = encodeURI('Lokasi');
    window.open('geo:0,0?q=' + destination, '_system');
  }

  dikirim(val_id) {
    this.loading.present();

    this.pesananStatus = {
      id: val_id,
      status: 'Dikirim'
    }

    this.pesananService.updatePesananStatus(this.pesananStatus).then(() => {
      this.load_data();
    }, err => {
      // this.showToast('Gagal mengupdate data.');
      console.error(JSON.stringify(err));
      this.loading.dismiss();
    });
  }

  selesai(val_id) {
    this.loading.present();

    this.pesananStatus = {
      id: val_id,
      status: 'Selesai'
    }

    this.pesananService.updatePesananStatus(this.pesananStatus).then(() => {
      this.load_data();
    }, err => {
      // this.showToast('Gagal mengupdate data.');
      console.error(JSON.stringify(err));
      this.loading.dismiss();
    });
  }

}
