import { Component, OnInit } from '@angular/core';
import { PesananService, Pesanan } from '../../../services/pesanan.service';
import { AkunService, Akun } from '../../../services/akun.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-pesanan-riwayat',
  templateUrl: './pesanan-riwayat.page.html',
  styleUrls: ['./pesanan-riwayat.page.scss'],
})
export class PesananRiwayatPage implements OnInit {

  data: any = [];

  loading: any;

  pesananStatus: any = {
    id: '',
    status: '',
  };

  sec: number = 0;

  constructor(
    public akunService: AkunService,
    public loadingController: LoadingController,
    public pesananService: PesananService,
  ) { }

  async ngOnInit() {
    this.loading = await this.loadingController.create({})
  }

  ionViewDidEnter() {
    this.load_data();
  }

  load_data() {
    this.data = [];

    let nama: string = '';
    let nomor_hp: string = '';

    this.pesananService.getpesanan().subscribe(snap_pesanan => {
      this.akunService.getAkun().subscribe(snap_akun => {
        if (snap_akun[ 0 ].id == snap_pesanan[ 0 ].id_pelanggan) {
          nama = snap_akun[ 0 ].nama;
          nomor_hp = snap_akun[ 0 ].nomor_hp;
        }

        this.data.push({
          pembeli_nama: nama,
          pembeli_nomor_hp: nomor_hp,
          data: snap_pesanan[ 0 ]
        });

        this.loading.dismiss();

        // alert(JSON.stringify(this.data));
      });
    });
  }

  launch_map() {
    let destination = this.data[ 0 ].lokasi.latitude + ',' + this.data[ 0 ].lokasi.longitude;

    // let label = encodeURI('Lokasi');
    // window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');

    let label = encodeURI('Lokasi');
    window.open('geo:0,0?q=' + destination, '_system');
  }
}
