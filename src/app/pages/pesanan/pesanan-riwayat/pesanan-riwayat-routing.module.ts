import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PesananRiwayatPage } from './pesanan-riwayat.page';

const routes: Routes = [
  {
    path: '',
    component: PesananRiwayatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PesananRiwayatPageRoutingModule {}
