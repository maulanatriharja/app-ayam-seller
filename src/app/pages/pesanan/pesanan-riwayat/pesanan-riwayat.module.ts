import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PesananRiwayatPageRoutingModule } from './pesanan-riwayat-routing.module';

import { PesananRiwayatPage } from './pesanan-riwayat.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PesananRiwayatPageRoutingModule
  ],
  declarations: [PesananRiwayatPage]
})
export class PesananRiwayatPageModule {}
