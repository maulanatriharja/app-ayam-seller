import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProdukService, Produk } from '../../../services/produk.service';
import { ActionSheetController, AlertController, LoadingController, NavController, Platform, ToastController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import * as firebase from 'firebase';

@Component({
  selector: 'app-produk-detail',
  templateUrl: './produk-detail.page.html',
  styleUrls: [ './produk-detail.page.scss' ],
})
export class ProdukDetailPage implements OnInit {

  header_title: string = "Tambah Produk";
  photo: string = "assets/images/noimage.png";
  captureDataUrl: string;

  loading: any;

  // produk: Produk = {
  //   nama_produk: '',
  //   harga_modal: '',
  //   harga_jual: '',
  //   keterangan: '',
  //   foto: ''
  // };

  produk: any = {
    nama_produk: '',
    harga_modal: '',
    harga_jual: '',
    keterangan: '',
  };

  constructor(
    public actionSheetController: ActionSheetController,
    public activatedRoute: ActivatedRoute,
    public alertController: AlertController,
    public camera: Camera,
    public loadingController: LoadingController,
    public navCtrl: NavController,
    public plt: Platform,
    public produkService: ProdukService,
    public route: ActivatedRoute,
    public router: Router,
    public toastCtrl: ToastController,

  ) { }

  async ngOnInit() {
    this.loading = await this.loadingController.create({})

    this.route.queryParams.subscribe(params => {
      this.produk = JSON.parse(params.data);

      if (this.produk) {
        this.header_title = "Detail Produk";

        let storageRef = firebase.storage().ref();
        let imageRef = storageRef.child('produk/' + this.produk.id + '.jpg');

        imageRef.getDownloadURL().then(function (url) {
          document.getElementById('photo').style.backgroundImage = "url('" + url + "')";
        })
      }
    });
  }

  ionViewDidEnter() {
    //   this.plt.ready().then(() => {

    //   });
  }

   addProduk() {
     this.loading.present();

    this.produkService.addProduk(this.produk).then((snap) => {
      if (this.photo != "assets/images/noimage.png") {
        this.upload_photo(snap.id)
      } else {
        this.showToast('Data baru berhasil ditambahkan.');
        this.navCtrl.back();
        // this.produkService.pubDataProduk({});
        this.loading.dismiss();
      }

    }, err => {
      this.showToast('Gagal menambahkan data.');
      console.error(JSON.stringify(err));
      this.loading.dismiss();
    });
  }

   updateProduk() {
     this.loading.present();

    this.produkService.updateProduk(this.produk).then(() => {
      if (this.photo != "assets/images/noimage.png") {
        this.upload_photo(this.produk.id)
      } else {
        this.showToast('Data baru berhasil ditambahkan.');
        this.navCtrl.back();
        // this.produkService.pubDataProduk({});
        this.loading.dismiss();
      }

    }, err => {
      this.showToast('Gagal mengupdate data.');
      console.error(JSON.stringify(err));
      this.loading.dismiss();
    });
  }

  async deleteProduk() {
    const alert = await this.alertController.create({
      header: 'Hapus Data',
      message: 'Yakin menghapus data <strong>' + this.produk.nama_produk + '</strong> ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (h) => {
            //
          }
        }, {
          text: 'Hapus',
          handler: () => {
            this.produkService.deleteProduk(this.produk.id).then(() => {
              this.showToast('Data telah dihapus');
              this.navCtrl.back();
            }, err => {
              this.showToast('Gagal menghapus data');
            });

          }
        }
      ]
    });

    await alert.present();
  }

  showToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 2000,
      position: 'top',
    }).then(toast => toast.present());
  }

  async selectImageSource() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Photo',
      buttons: [ {
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          console.info('Open Camera');
          this.openCamera();
        }
      }, {
        text: 'Gallery',
        icon: 'images',
        handler: () => {
          console.info('Open Gallery');
          this.openGallery();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      } ]
    });
    await actionSheet.present();
  }

  openCamera() {
    const cameraOptions: CameraOptions = {
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      quality: 100,
      targetHeight: 300,
      targetWidth: 300,
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.photo = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.error('err : ' + err);
      alert('err : ' + JSON.stringify(err))

      this.photo = "assets/images/noimage.png";
    });
  }

  openGallery() {
    let options = {
      allowEdit: true,
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetHeight: 300,
      targetWidth: 300,
    };

    this.camera.getPicture(options).then((imagePath) => {
      this.photo = 'data:image/jpeg;base64,' + imagePath;
    }, (err) => {
      console.error('err : ' + err);
      alert('err : ' + JSON.stringify(err))

      this.photo = "assets/images/noimage.png";
    });
  }

  upload_photo(val_id) {
    this.captureDataUrl = this.photo;

    let storageRef = firebase.storage().ref();
    let imageRef = storageRef.child('produk/' + val_id + '.jpg');

    imageRef.putString(this.captureDataUrl, firebase.storage.StringFormat.DATA_URL).then((snapshot) => {
      this.showToast('Data baru berhasil ditambahkan.');
      this.navCtrl.back();
      // this.produkService.pubDataProduk({});
      this.loading.dismiss();
    });
  }
}