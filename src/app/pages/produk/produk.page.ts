import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProdukService, Produk } from '../../services/produk.service';
import { Platform } from '@ionic/angular';


@Component({
  selector: 'app-produk',
  templateUrl: './produk.page.html',
  styleUrls: [ './produk.page.scss' ],
})
export class ProdukPage implements OnInit {

  // public produks: Observable<Produk[]>;
  public produks: any = [];
  sec: number = 0;

  skl_array: any = [ 0, 1, 2, 3, 4, 5 ];

  constructor(
    public plt: Platform,
    public produkService: ProdukService,
    public router: Router,
  ) {
    // this.plt.ready().then(() => {
    // this.produkService.obvDataProduk().subscribe(() => {
    //   this.loadData();
    // });
    // });
  }

  ngOnInit() {
    // this.loadData();

    // this.produkService.obvDataProduk().subscribe(() => {
    //   this.loadData();
    // });
  }

  ionViewDidEnter() {
    this.loadData();
  }

  loadData() {
    this.sec = new Date().getSeconds();

    // this.produks = this.produkService.getproduk();

    this.produks = [];

    this.produkService.getproduk().subscribe(snap => {
      this.produks = snap;
    });
  }

  doRefresh(event) {
    console.log('Begin async operation');

    this.loadData();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  detailProduk(val_id) {
    this.router.navigate([ '/produk-detail' ],
      {
        queryParams: {
          data: JSON.stringify(val_id)
        }
      }
    );
  }
}
