import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'beranda',
    loadChildren: () => import('./pages/beranda/beranda.module').then( m => m.BerandaPageModule)
  },
  {
    path: 'produk-detail',
    loadChildren: () => import('./pages/produk/produk-detail/produk-detail.module').then( m => m.ProdukDetailPageModule)
  },
  {
    path: 'pesanan',
    loadChildren: () => import('./pages/pesanan/pesanan.module').then( m => m.PesananPageModule)
  },
  {
    path: 'pesanan-riwayat',
    loadChildren: () => import('./pages/pesanan/pesanan-riwayat/pesanan-riwayat.module').then( m => m.PesananRiwayatPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then( m => m.AkunPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/beranda/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'produk',
    loadChildren: () => import('./pages/produk/produk.module').then( m => m.ProdukPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
