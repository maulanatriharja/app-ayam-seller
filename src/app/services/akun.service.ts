import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';

export interface Akun {
  id?: string,
  nama: string,
  nomor_hp: string,
}

export interface Password {
  id?: string,
  password: string,
}

@Injectable({
  providedIn: 'root'
})
export class AkunService {

  private akun: Observable<Akun[]>;
  private akunCollection: AngularFirestoreCollection<Akun>;

  private data_akun = new Subject<any>();
  pubAkun(data: any) { this.data_akun.next(data); }
  obvAkun(): Subject<any> { return this.data_akun; }

  constructor(
    private afs: AngularFirestore
  ) {
    this.akunCollection = this.afs.collection<Akun>('akun');
    this.akun = this.akunCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getAkun(): Observable<Akun[]> {
    return this.akun;
  }

  // getAkun1(id: string): Observable<Akun> {
  //   return this.akunCollection.doc<Akun>(id).valueChanges().pipe(
  //     take(1),
  //     map(akun => {
  //       akun.id = id;
  //       return akun
  //     })
  //   );
  // }

  addAkun(akun: Akun): Promise<DocumentReference> {
    return this.akunCollection.add(akun);
  }

  updateAkun(akun: Akun): Promise<void> {
    return this.akunCollection.doc(akun.id).update({
      nama: akun.nama,
      nomor_hp: akun.nomor_hp,
    });
  }

  updatePassword(pass: Password): Promise<void> {
    return this.akunCollection.doc(pass.id).update({
      password: pass.password,
    });
  }

  deleteAkun(id: string): Promise<void> {
    return this.akunCollection.doc(id).delete();
  }
}