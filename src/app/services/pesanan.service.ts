import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Pesanan {
  id?: string,
  id_pelanggan: string,
  nota: string,
}

export interface Status {
  id?: string,
  status: string,
}

@Injectable({
  providedIn: 'root'
})
export class PesananService {

  private pesanan: Observable<Pesanan[]>;
  private pesananCollection: AngularFirestoreCollection<Pesanan>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.pesananCollection = this.afs.collection<Pesanan>('pesanan');
    this.pesanan = this.pesananCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getpesanan(): Observable<Pesanan[]> {
    return this.pesanan;
  }

  getPesanan(id: string): Observable<Pesanan> {
    return this.pesananCollection.doc<Pesanan>(id).valueChanges().pipe(
      take(1),
      map(pesanan => {
        pesanan.id = id;
        return pesanan
      })
    );
  }

  addPesanan(pesanan: Pesanan): Promise<DocumentReference> {
    return this.pesananCollection.add(pesanan);
  }

  updatePesananStatus(st: Status): Promise<void> {
    return this.pesananCollection.doc(st.id).update({
      status: st.status,
    });
  }

  /* deletePesanan(id: string): Promise<void> {
    return this.pesananCollection.doc(id).delete();
  } */
}