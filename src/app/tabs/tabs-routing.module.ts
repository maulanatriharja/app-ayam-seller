import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'beranda',
        loadChildren: () => import('../pages/beranda/beranda.module').then(m => m.BerandaPageModule)
      },
      {
        path: 'produk',
        loadChildren: () => import('../pages/produk/produk.module').then(m => m.ProdukPageModule)
      },
      {
        path: 'pesanan',
        loadChildren: () => import('../pages/pesanan/pesanan.module').then(m => m.PesananPageModule)
      },
      {
        path: 'akun',
        loadChildren: () => import('../pages/akun/akun.module').then(m => m.AkunPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/beranda',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/beranda',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class TabsPageRoutingModule { }
